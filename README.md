# Tuffet

Boiler plate for website design.

This is primarily a personal project to put together a base template for future of web design projects.

* [GSS](http://gss.github.io/guides/ccss) Constraint Based CSS 
* [Webpack](https://webpack.js.org/)

For GSS we pull down the [minified script](https://s3-us-west-2.amazonaws.com/cdn.thegrid.io/gss/v2.0.0/v2.0.0/gss.min.js).
Not sure if it is available as an NPM package.

